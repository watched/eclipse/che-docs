[id="proc_collecting-dev-workspace-operator-metrics-with-prometheus_{context}"]
= Collecting {devworkspace} Operator metrics with Prometheus

[role="_abstract"]
To use Prometheus to collect, store, and query metrics about the {devworkspace} Operator:

.Prerequisites

* The link:https://github.com/devfile/devworkspace-operator/blob/v0.10.0/deploy/deployment/kubernetes/objects/devworkspace-controller-metrics.Service.yaml[`devworkspace-controller-metrics` Service] is exposing metrics on port `8443`.

* The `devworkspace-webhookserver` Service is exposing metrics on port `9443`. By default, the Service exposes metrics on port `9443`.

* Prometheus 2.26.0 or later is running. The Prometheus console is running on port `9090` with a corresponding `service` and `route`. See link:https://prometheus.io/docs/introduction/first_steps/[First steps with Prometheus].

.Procedure

. Create a `ClusterRoleBinding` to bind the `ServiceAccount` associated with Prometheus to the link:https://github.com/devfile/devworkspace-operator/blob/main/deploy/deployment/kubernetes/objects/devworkspace-controller-metrics-reader.ClusterRole.yaml[devworkspace-controller-metrics-reader] `ClusterRole`.
+
NOTE: Without the `ClusterRoleBinding`, you cannot access {devworkspace} metrics because access is protected with role-based access control (RBAC).
+
.ClusterRole
====
[source,yaml,subs="+quotes,+attributes,+macros"]
----
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: devworkspace-controller-metrics-reader
rules:
- nonResourceURLs:
  - /metrics
  verbs:
  - get
----
====
+
.ClusterRoleBinding
====
[source,yaml,subs="+quotes,+attributes,+macros"]
----
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: devworkspace-controller-metrics-binding
subjects:
  - kind: ServiceAccount
    name: __<ServiceAccount_name_associated_with_the_Prometheus_Pod>__
    namespace: __<Prometheus_namespace>__
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: devworkspace-controller-metrics-reader
----
====

. Configure Prometheus to scrape metrics from port `8443` exposed by the `devworkspace-controller-metrics` Service and from port `9443` exposed by the `devworkspace-webhookserver` Service.
+
.Prometheus configuration
====
[source,yaml,subs="+quotes,+attributes,+macros"]
----
apiVersion: v1
kind: ConfigMap
metadata:
  name: prometheus-config
data:
  prometheus.yml: |-
      global:
        scrape_interval: 5s <1>
        evaluation_interval: 5s <2>
      scrape_configs: <3>
        - job_name: 'DevWorkspace'
          scheme: https
          authorization:
            type: Bearer
            credentials_file: '/var/run/secrets/kubernetes.io/serviceaccount/token'
          tls_config:
            insecure_skip_verify: true
          static_configs:
            - targets: ['devworkspace-controller-metrics:8443'] <4>
        - job_name: 'DevWorkspace webhooks'
          scheme: https
          authorization:
            type: Bearer
            credentials_file: '/var/run/secrets/kubernetes.io/serviceaccount/token'
          tls_config:
            insecure_skip_verify: true
          static_configs:
            - targets: ['devworkspace-webhookserver:9443'] <5>
----
<1> The rate at which a target is scraped.
<2> The rate at which the recording and alerting rules are re-checked.
<3> The resources that Prometheus monitors. In the default configuration, two jobs, `DevWorkspace` and `DevWorkspace webhooks`, scrape the time series data exposed by the `devworkspace-controller-metrics` and `devworkspace-webhookserver` Services.
<4> The scrape metrics from port `8443`.
<5> The scrape metrics from port `9443`.
====

.Verification steps

. Use the Prometheus console to view and query metrics:
** View the metrics at `http://__<prometheus_url>__/metrics`.
** Query metrics from `http://__<prometheus_url>__/graph`.
+
For more information, see link:https://prometheus.io/docs/introduction/first_steps/#using-the-expression-browser[Using the expression browser].
. Verify that all targets are up by viewing the targets endpoint at `http://__<prometheus-url>__/targets`.


[role="_additional-resources"]
.Additional resources

* link:https://prometheus.io/docs/prometheus/latest/configuration/configuration/[Configuring Prometheus]

* link:https://prometheus.io/docs/prometheus/latest/querying/basics/[Querying Prometheus]

* link:https://prometheus.io/docs/concepts/metric_types/[Prometheus metric types]
